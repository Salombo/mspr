package com.mspr.dev.controller;

import com.mspr.dev.controllers.LoginPageController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

/**
 * Classe permettant la recherche dans l'annuaire LDAP
 * Snippet largement inspiré par : https://www.codeproject.com/Articles/653337/Java-Retrieving-User-Informatio
 */
@Component
public class ActiveDirectoryFinder
{
    private static final Logger logger = LoggerFactory.getLogger(LoginPageController.class);
    //Attribute names
    private static final String AD_ATTR_NAME_TOKEN_GROUPS = "tokenGroups";
    private static final String AD_ATTR_NAME_OBJECT_CLASS = "objectClass";
    private static final String AD_ATTR_NAME_OBJECT_CATEGORY = "objectCategory";
    private static final String AD_ATTR_NAME_MEMBER = "member";
    private static final String AD_ATTR_NAME_MEMBER_OF = "memberOf";
    private static final String AD_ATTR_NAME_DESCRIPTION = "description";
    private static final String AD_ATTR_NAME_OBJECT_GUID = "objectGUID";
    private static final String AD_ATTR_NAME_OBJECT_SID = "objectSid";
    private static final String AD_ATTR_NAME_DISTINGUISHED_NAME = "distinguishedName";
    private static final String AD_ATTR_NAME_CN = "cn";
    private static final String AD_ATTR_NAME_USER_PRINCIPAL_NAME = "userPrincipalName";
    private static final String AD_ATTR_NAME_USER_EMAIL = "mail";
    private static final String AD_ATTR_NAME_GROUP_TYPE = "groupType";
    private static final String AD_ATTR_NAME_SAM_ACCOUNT_TYPE = "sAMAccountType";
    private static final String AD_ATTR_NAME_USER_ACCOUNT_CONTROL = "userAccountControl";

    /**
     * Recherche de l'adresse mail de l'utilisateur
     * @param ctx compte authentifié
     * @param searchBase base de recherche de la requête
     * @param domainWithUser "DOMAINE\Utilisateur"
     * @return String
     */
    public String getUserMailByDomainWithUser(LdapContext ctx, String searchBase, String domainWithUser)
    {
        logger.debug("trying to get email of domainWithUser " + domainWithUser + " using baseDN " + searchBase);
        String userName = domainWithUser.substring(domainWithUser.indexOf('\\') +1 );
        try
        {
            NamingEnumeration<SearchResult>
                    userDataBysAMAccountName = getUserDataBysAMAccountName(ctx, searchBase, userName);
            return getUserMailFromSearchResults( userDataBysAMAccountName );
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Recherche des données utilisateur via le nom de l'utilisateur
     * @param ctx compte authentifié
     * @param searchBase base de recherche de la requête
     * @param username nom de l'utilisateur
     * @return NamingEnumeration<SearchResult>
     * @throws Exception
     */
    private NamingEnumeration<SearchResult>
    getUserDataBysAMAccountName(LdapContext ctx, String searchBase, String username)
            throws Exception
    {
        //String filter = "(&(sAMAccountName=" + username + "))";
        String filter = "(&(&(objectClass=person)(objectCategory=user))(sAMAccountName=" + username + "))";
        SearchControls searchCtls = new SearchControls();
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        NamingEnumeration<SearchResult> answer = null;
        try
        {
            answer = ctx.search(searchBase, filter, searchCtls);
        }
        catch (Exception e)
        {
            logger.error("Error searching Active directory for " + filter);
            throw e;
        }

        return answer;
    }

    /**
     * On se sert du résultat de la recherche pour en sortir l'adresse mail
     * @param userData sortie de getUserDataBysAMAccountName()
     * @return String adresse mail
     * @throws Exception
     */
    private String getUserMailFromSearchResults( NamingEnumeration<SearchResult> userData )
            throws Exception
    {
        try
        {
            String mail = null;
            // getting only the first result if we have more than one
            if (userData.hasMoreElements())
            {
                SearchResult sr = userData.nextElement();
                Attributes attributes = sr.getAttributes();
                mail = attributes.get(AD_ATTR_NAME_USER_EMAIL).get().toString();
                logger.debug("found email " + mail);
            }

            return mail;
        }
        catch (Exception e)
        {
            logger.error("Error fetching attribute from object");
            throw e;
        }
    }
}