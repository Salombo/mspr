package com.mspr.dev.controller;

/**
 * Classe de stockage du OTP envoyé
 */
public class OtpPost {
    private String otp;

    public String getOtp(){
        return otp;
    }

    public void setOtp(String otp){
        this.otp = otp;
    }
}
