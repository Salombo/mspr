package com.mspr.dev.controller;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.PreparedStatement;

/**
 * Classe de requêtage vers l'API de détection de compte compromis
 */
public class HaveIBeenPwned {
    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    /**
     * Vérifie via l'API HaveIBeenPwned si le compte est compromis
     * @param email adresse email de l'utilisateur
     * @return boolean
     * @throws Exception
     */
    public boolean isAccountPwned(String email) throws Exception {
        //email = "adrien.lenoire@orange.fr";
        String uri = "https://haveibeenpwned.com/api/v3/breachedaccount/"+email;

        HttpGet request = new HttpGet(uri);

        request.addHeader("hibp-api-key", "3a08457e8c3d49f2a242dc74481d76a7");
        request.addHeader(HttpHeaders.USER_AGENT, "MSPR-DEV-API");

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            System.out.println(response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            Header headers = entity.getContentType();

            if (entity != null) {
                String result = EntityUtils.toString(entity);

                JSONParser jsonParser = new JSONParser();
                JSONArray jsonArray = (JSONArray)jsonParser.parse(result);

                if(jsonArray == null)
                    return false;

                return jsonArray.size() > 0;

            }

        } catch(Exception e){
            System.out.println("Except : " + e.getMessage());
        }
        return false;
    }
}
