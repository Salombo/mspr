package com.mspr.dev.controller;

/**
 * Classe de stockage des informations fournies dans l'authentification
 */
public class LoginPost {
    private String identifiant;
    private String password;

    public String getIdentifiant(){
        return identifiant;
    }

    public void setIdentifiant(String identifiant){
        this.identifiant = identifiant;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }
}
