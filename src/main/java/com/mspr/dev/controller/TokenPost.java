package com.mspr.dev.controller;

/**
 * Classe de stockage du token envoyé
 */
public class TokenPost {
    private String token;

    public String getToken(){
        return token;
    }

    public void setToken(String token){
        this.token = token;
    }
}
