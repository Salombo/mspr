package com.mspr.dev.controller;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Gestion de l'envoi de mail
 */
public class JavaMailServ {
    /**
     * Initialisation de la connexion au service mail
     * @return JavaMailSender
     */
    private static JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("otarie.mspr@gmail.com");
        mailSender.setPassword("Admin@MSPR");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    /**
     * Envoi de mail
     * @param to Destinataire du mail
     * @param sujet Sujet du mail
     * @param body Corps du mail
     */
    public static void sendMessage(String to, String sujet, String body){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(sujet);
        message.setText(body);

        getJavaMailSender().send(message);
    }
}
