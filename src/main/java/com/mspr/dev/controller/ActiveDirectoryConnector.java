package com.mspr.dev.controller;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;

/**
 * Connecteur au serveur LDAP
 */
public class ActiveDirectoryConnector {


    public static LdapContext login(String username, String password, String serverName, String domainName) {

        Hashtable<String, String> infos = new Hashtable<>();
        infos.put(Context.SECURITY_AUTHENTICATION, "simple");
        infos.put(Context.SECURITY_PRINCIPAL, username + "@" + domainName);
        infos.put(Context.SECURITY_CREDENTIALS, password);
        infos.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        infos.put(Context.PROVIDER_URL, "ldap://" + serverName + ":389");

        LdapContext ctx = null;
        try {
            ctx = new InitialLdapContext(infos, null);
        } catch (CommunicationException e) {
            System.out.println("Erreur de connexion au serveur");
        } catch (NamingException e) {
            System.out.println("Mauvais identifiant");
        }

        return ctx;
    }
}
