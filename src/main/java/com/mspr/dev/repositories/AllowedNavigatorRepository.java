package com.mspr.dev.repositories;

import com.mspr.dev.entities.AllowedIp;
import com.mspr.dev.entities.AllowedNavigator;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AllowedNavigatorRepository extends CrudRepository<AllowedNavigator, Integer> {
    List<AllowedNavigator> findByUtilisateurId(int id);
    List<AllowedNavigator> findByUtilisateurIdAndNavigateur(int id, String navigateur);
}
