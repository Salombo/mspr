package com.mspr.dev.repositories;

import com.mspr.dev.entities.Utilisateur;
import org.springframework.data.repository.CrudRepository;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer> {

}
