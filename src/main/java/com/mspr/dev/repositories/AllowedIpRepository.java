package com.mspr.dev.repositories;

import com.mspr.dev.entities.AllowedIp;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AllowedIpRepository extends CrudRepository<AllowedIp, Integer> {
    List<AllowedIp> findByUtilisateurId(int id);
    List<AllowedIp> findByUtilisateurIdAndIpAddress(int id, String ipAddress);
}
