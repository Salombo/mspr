package com.mspr.dev.repositories;

import com.mspr.dev.entities.UtilisateurToken;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UtilisateurTokenRepository extends CrudRepository<UtilisateurToken, Integer> {
    List<UtilisateurToken> findByUtilisateurId(int id);
}
