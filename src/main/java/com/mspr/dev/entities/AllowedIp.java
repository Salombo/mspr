package com.mspr.dev.entities;
import javax.persistence.*;

@Entity
public class AllowedIp {
    @Id
    @GeneratedValue (strategy= GenerationType.AUTO)
    private int id;

    @Column(length = 64)
    private String ipAddress;

    @Column(length = 64)
    private String countryName;

    @ManyToOne
    private Utilisateur utilisateur;

    public Integer getId(){
        return id;
    }

    public Utilisateur getUtilisateur(){
        return utilisateur;
    }

    public String getIp(){
        return ipAddress;
    }

    public void setIp(String ip){
        this.ipAddress = ip;
    }

    public String getCountryName(){
        return countryName;
    }

    public void setCountryName(String countryName){
        this.countryName = countryName;
    }

    public void setUtilisateur(Utilisateur utilisateur){
        this.utilisateur = utilisateur;
    }
}
