package com.mspr.dev.entities;


import javax.persistence.*;
import java.util.Set;

@Entity
public class Utilisateur {
    @Id
    @GeneratedValue (strategy= GenerationType.AUTO)
    private Integer id;

    @Column(length = 256)
    private String userid;

    @Column(length = 512)
    private String token_validator;

    @OneToMany(mappedBy = "utilisateur", cascade = CascadeType.PERSIST)
    private Set<AllowedIp> allowedIps;

    @OneToMany(mappedBy = "utilisateur", cascade = CascadeType.PERSIST)
    private Set<AllowedNavigator> allowedNavigators;

    public Integer getId(){
        return id;
    }

    public String getUserid(){
        return userid;
    }

    public void setUserid(String userid){
        this.userid = userid;
    }

    public String getToken_validator(){
        return token_validator;
    }

    public void setToken_validator(String token_validator){
        this.token_validator = token_validator;
    }

    /*public void addIp(AllowedIp allowedIp){
        this.allowedIps.add(allowedIp);
    }
    public void addNavigator(AllowedNavigator allowedNavigator){
        this.allowedNavigators.add(allowedNavigator);
    }*/

}
