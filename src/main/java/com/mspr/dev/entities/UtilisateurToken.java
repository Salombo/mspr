package com.mspr.dev.entities;
import com.mspr.dev.repositories.UtilisateurRepository;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Entity
public class UtilisateurToken {
    @Id
    @GeneratedValue (strategy= GenerationType.AUTO)
    private int id;

    @Column(length = 512)
    private String token;

    @Column
    private Date expire;

    @OneToOne
    private Utilisateur utilisateur;

    public Integer getId(){
        return id;
    }

    public String getToken(){
        return token;
    }

    public Date getExpire(){
        return expire;
    }

    public void setToken(String token){
        this.token = token;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setExpire(Date expire){
        this.expire = expire;
    }

    public Utilisateur getUtilisateur(){
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur){
        this.utilisateur = utilisateur;
    }

    /**
     * Génération d'un token utilisateur permettant son authentification à travers le réseau
     * @return String token
     */
    public String generateToken(){
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());

            String uuid = UUID.randomUUID().toString();

            int randomNum = ThreadLocalRandom.current().nextInt(1000, 8000);

            String generatedToken = DigestUtils.md5Hex(ts + "" + randomNum + utilisateur.getUserid() + "vvwPt%-_sts" + uuid + "OtArIeMsPrmSpRABSR$%[" + ts).toUpperCase();

            return generatedToken;
    }
}
