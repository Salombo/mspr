package com.mspr.dev.entities;

import javax.persistence.*;

@Entity
public class AllowedNavigator {
    @Id
    @GeneratedValue
    private int id;

    @Column(length = 256)
    private String navigateur;

    @ManyToOne
    private Utilisateur utilisateur;

    public Integer getId(){
        return id;
    }

    public String getNavigateur(){
        return navigateur;
    }

    public Utilisateur getUtilisateur(){
        return utilisateur;
    }

    public void setNavigateur(String navigateur){
        this.navigateur = navigateur;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setUtilisateur(Utilisateur utilisateur){
        this.utilisateur = utilisateur;
    }
}
