package com.mspr.dev.controllers;


import com.mspr.dev.controller.ActiveDirectoryConnector;
import com.mspr.dev.controller.ActiveDirectoryFinder;
import com.mspr.dev.controller.LoginPost;
import com.mspr.dev.controller.TokenPost;
import com.mspr.dev.entities.AllowedIp;
import com.mspr.dev.entities.AllowedNavigator;
import com.mspr.dev.entities.Utilisateur;
import com.mspr.dev.repositories.AllowedIpRepository;
import com.mspr.dev.repositories.AllowedNavigatorRepository;
import com.mspr.dev.repositories.UtilisateurRepository;
import io.ipinfo.api.IPInfo;
import io.ipinfo.api.errors.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class TokenPageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenPageController.class);

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private AllowedIpRepository allowedIpRepository;

    @Autowired
    AllowedNavigatorRepository allowedNavigatorRepository;

    /**
     * Controller par lequel la requête GET /token est gérée
     * @param model permet d'insérer des notions de datas qui seront mappées dans resources/templates
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String retourne un "String" qui correspond à un fichier html situé dans resources/templates
     */
    @GetMapping("/token")
    public String loginForm(Model model, HttpServletRequest req) {

        model.addAttribute("tokenPost", new TokenPost());
        return "token";
    }

    /**
     * Controller par lequel la requête POST /token est gérée
     * @param tokenPost contient les informations saisies dans le formulaire Token
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String
     * @throws NamingException
     */
    @PostMapping("/token")
    public String loginPost(TokenPost tokenPost, HttpServletRequest req) throws NamingException {
        if (checkToken(req, tokenPost))
            return "redirect:/login";
        else
            return "token";
    }

    /**
     * Processus de vérification de la validité du token saisi
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @param tokenPost contient les informations saisies dans le formulaire Token
     * @return boolean
     */
    private boolean checkToken(HttpServletRequest req, TokenPost tokenPost){
        if(tokenPost.getToken() == null)
            return false;

        var userAgent = req.getHeader("User-Agent");

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setToken_validator(tokenPost.getToken());

        AllowedNavigator allowedNavigator = new AllowedNavigator();
        allowedNavigator.setNavigateur(getBrowser(userAgent));

        AllowedIp allowedIp = new AllowedIp();
        allowedIp.setIp(req.getHeader("x-forwarded-for"));

        IPInfo ipInfo = IPInfo.builder().setToken("9c72b00404585e").build();

        try{
            allowedIp.setCountryName(ipInfo.lookupIP(req.getHeader("x-forwarded-for")).getCountryCode());
        }catch (RateLimitedException ex){
            LOGGER.error("Limite atteinte");
        }

        Utilisateur usr = retrieveUser(utilisateur, utilisateurRepository);

        if(usr == null)
            return false;
        else{
            usr.setToken_validator(null);
            utilisateurRepository.save(usr);

            if(isNewIp(usr, allowedIp, allowedIpRepository)){
                allowedIp.setUtilisateur(usr);
                allowedIpRepository.save(allowedIp);
            }

            if(isNewNavigator(usr, allowedNavigator, allowedNavigatorRepository)){
                allowedNavigator.setUtilisateur(usr);
                allowedNavigatorRepository.save(allowedNavigator);
            }
        }

        return true;
    }

    /**
     * Cette fonction permet de retrouver un utilisateur à partir de son token de validation
     * @param utilisateur utilisateur concerné par la recherche
     * @param utilisateurRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return Utilisateur l'ensemble des informations nécessaires
     */
    private Utilisateur retrieveUser(Utilisateur utilisateur, UtilisateurRepository utilisateurRepository){
        Iterable<Utilisateur> users = utilisateurRepository.findAll();

        for(Utilisateur user : users){
            if(user.getToken_validator() != null && user.getToken_validator().equals(utilisateur.getToken_validator())){
                return user;
            }
        }

        return null;
    }

    /**
     * Vérifie si l'IP est nouvelle pour l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on fait la recherche
     * @param ipAddr adresse IP utilisée par l'utilisateur
     * @param allowedIpRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return
     */
    private boolean isNewIp(Utilisateur utilisateur, AllowedIp ipAddr, AllowedIpRepository allowedIpRepository){
        List<AllowedIp> list = allowedIpRepository.findByUtilisateurIdAndIpAddress(utilisateur.getId(), ipAddr.getIp());

        return list.size() <= 0;
    }

    /**
     * Vérifie si le navigateur est un nouveau navigateur pour l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on fait la recherche
     * @param allowedNavigator navigateur utilisé par l'utilisateur
     * @param allowedNavigatorRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return Utilisateur l'ensemble des informations nécessaires
     * @return
     */
    private boolean isNewNavigator(Utilisateur utilisateur, AllowedNavigator allowedNavigator, AllowedNavigatorRepository allowedNavigatorRepository){
        List<AllowedNavigator> list = allowedNavigatorRepository.findByUtilisateurIdAndNavigateur(utilisateur.getId(), allowedNavigator.getNavigateur());

        return list.size() <= 0;
    }

    /**
     * Permet d'identifier le navigateur utilisé par l'utilisateur
     * Inspiré par https://stackoverflow.com/questions/1326928/how-can-i-get-client-information-such-as-os-and-browser/18030465
     * @param userAgent signature du navigateur
     * @return String
     */
    private String getBrowser(String userAgent){
        String user =   userAgent.toLowerCase();

        String browser = "";

        System.out.println("User Agent for the request is===>"+userAgent);
        //===============Browser===========================
        if (user.contains("msie"))
        {
            browser="IE";
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser="SAFARI";
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera") || user.contains("opr"))
                browser="OPERA";
        } else if (user.contains("chrome"))
        {
            browser="CHROME";
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape";

        } else if (user.contains("firefox"))
        {
            browser="FIREFOX";
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = user;
        }
        System.out.println("Browser Name==========>"+browser);

        return browser;
    }
}