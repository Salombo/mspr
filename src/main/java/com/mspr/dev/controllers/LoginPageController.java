package com.mspr.dev.controllers;

import com.mspr.dev.controller.*;
import com.mspr.dev.entities.AllowedIp;
import com.mspr.dev.entities.AllowedNavigator;
import com.mspr.dev.entities.Utilisateur;
import com.mspr.dev.repositories.AllowedIpRepository;
import com.mspr.dev.repositories.AllowedNavigatorRepository;
import com.mspr.dev.repositories.UtilisateurRepository;
import io.ipinfo.api.IPInfo;
import io.ipinfo.api.errors.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.util.DigestUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

/**
 * Controller de la page /login
 */
@Controller
public class LoginPageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginPageController.class);

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private AllowedIpRepository allowedIpRepository;

    @Autowired
    AllowedNavigatorRepository allowedNavigatorRepository;

    @Autowired
    private ActiveDirectoryFinder adFinder;

    /**
     * Controller par lequel la requête GET /login est gérée
     * @param model permet d'insérer des notions de datas qui seront mappées dans resources/templates
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String retourne un "String" qui correspond à un fichier html situé dans resources/*
     * @throws NamingException
     */
   @GetMapping("/login")
    public String loginForm(Model model, HttpServletRequest req) {
       model.addAttribute("loginPost", new LoginPost());
        return "login";
    }

    /**
     * Controller par lequel la requête POST /login est gérée
     * @param loginPost contient les informations saisies dans le formulaire de connexion
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String retourne un "String" qui correspond à un fichier html situé dans resources/templates
     * @throws NamingException
     */
    @PostMapping("/login")
    public String loginPost(LoginPost loginPost, HttpServletRequest req) throws NamingException {
        LdapContext conn = ActiveDirectoryConnector.login(loginPost.getIdentifiant(), loginPost.getPassword(), "176.31.75.120", "CHATELET");

        loginPost.setIdentifiant(loginPost.getIdentifiant().toLowerCase());

        if(conn == null)
            return "login";

       if(checkUser(req, loginPost, conn)){
           return "redirect:/otp";
       }
       else {
           return "errorConn";
       }
    }

    /**
     * Cette fonction gère l'ensemble des vérifications nécessaires de l'utilisateur.
     * Au terme de l'exécution, on sait si l'utilisateur est ou non autorisé à se connecter
     * Récupération de l'adresse IP paramétrée pour être derrière un reverse proxy
     * @param req On se sert de ce paramètre pour travailler avec le contenu de la requête
     * @param loginPost contenu du formulaire envoyé par la requête POST
     * @param conn connexion LDAP effectuée en cas de succès
     * @return boolean true si toutes les conditions sont acceptées, false si une erreur a été rencontrée / vérification supplémentaire
     */
    private boolean checkUser(HttpServletRequest req, LoginPost loginPost, LdapContext conn){
        var userAgent = req.getHeader("User-Agent");

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUserid(loginPost.getIdentifiant());

        AllowedNavigator allowedNavigator = new AllowedNavigator();
        allowedNavigator.setNavigateur(getBrowser(userAgent));

        AllowedIp allowedIp = new AllowedIp();
        allowedIp.setIp(req.getHeader("x-forwarded-for"));

        IPInfo ipInfo = IPInfo.builder().setToken("9c72b00404585e").build();

        try{
            allowedIp.setCountryName(ipInfo.lookupIP(req.getHeader("x-forwarded-for")).getCountryCode());
        }catch (RateLimitedException ex){
            LOGGER.error("Limite atteinte");
        }

        String email = adFinder.getUserMailByDomainWithUser(conn, "dc=CHATELET,dc=otarie", "CHATELET\\" + loginPost.getIdentifiant());

        Utilisateur usr = retrieveUser(utilisateur, utilisateurRepository);
        if(usr == null){
            utilisateurRepository.save(utilisateur);

            usr = retrieveUser(utilisateur, utilisateurRepository);

            if(usr != null){
                allowedIp.setUtilisateur(usr);
                allowedIpRepository.save(allowedIp);

                allowedNavigator.setUtilisateur(usr);
                allowedNavigatorRepository.save(allowedNavigator);
            }
            else{
                LOGGER.error("Erreur après ajout !!");
            }
        } else {
            //Vérification de la partie adressage IP de l'utilisateur
            if (!ipIsValidated(usr, allowedIp.getIp(), allowedIpRepository)) {
                if (isIpCountryKnown(usr, allowedIp, allowedIpRepository))
                    JavaMailServ.sendMessage(email, "[AVERTISSEMENT] Connexion au portail via une nouvelle adresse IP", "Une connexion au portail a été réalisée avec une nouvelle adresse IP : " + allowedIp.getIp());
                else {
                    String token = generateCheckToken(usr, allowedIp, allowedNavigator, utilisateurRepository);
                    JavaMailServ.sendMessage(email, "[ALERTE] Connexion au portail via une nouvelle adresse IP", "Une connexion au portail a été réalisée avec une nouvelle adresse IP provenant d'un pays étranger : " + allowedIp.getIp());


                    JavaMailServ.sendMessage(email, "[TOKEN] Nouveau jeton de connexion", "Veuillez vous connecter à l'adresse suivante depuis votre poste avec le token ci-dessous : http://mspr-02.salombo.eu/token \n\n\n TOKEN : " + token);
                    return false;
                }
            }
            //Vérification de la partie navigateur de l'utilisateur
            if(!isNavigatorValidated(usr, allowedNavigator.getNavigateur(), allowedNavigatorRepository)){
                String token = generateCheckToken(usr, allowedIp, allowedNavigator, utilisateurRepository);
                JavaMailServ.sendMessage(email, "[ALERTE] Connexion au portail via un nouveau navigateur", "Une connexion au portail a été réalisée avec uneun nouveau navigateur : " + allowedNavigator.getNavigateur());


                JavaMailServ.sendMessage(email, "[TOKEN] Nouveau jeton de connexion", "Veuillez vous connecter à l'adresse suivante depuis votre poste avec le token ci-dessous : http://mspr-02.salombo.eu/token \n\n\n TOKEN : " + token);
                return false;
            }

            //On vérifie si l'utilisateur a été compromis
            HaveIBeenPwned haveIBeenPwned = new HaveIBeenPwned();

            try{
                if(haveIBeenPwned.isAccountPwned(email)){
                    JavaMailServ.sendMessage(email, "[ALERTE] Compte compromis", "Nous avons détecté que votre compte a été compromis, merci de vous rapprocher de l'équipe informatique afin de changer vos identifiants et mot de passe");
                }
            } catch(Exception e){
                LOGGER.error("Erreur : " + e.getMessage());
                return false;
            }
        }


        //Génration du OTP
        String OTP = generateOTPToken(usr == null ? utilisateur:usr, allowedIp, allowedNavigator, utilisateurRepository);
        JavaMailServ.sendMessage(email, "[OTP] Votre mot de passe unique", "Une connexion au portail a été initiée avec vos identifiants, vous retrouverez ci-dessous le mot de passe à usage unique : \n\n\n" + OTP);

       return true;
    }

    /**
     * Génère le token de vérification de l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on génère un token
     * @param allowedIp adresse IP de l'utilisateur
     * @param allowedNavigator navigateur de l'utilisateur
     * @param utilisateurRepository repository de la base de données permettant de stocker la data
     * @return String le token généré
     */
    private String generateCheckToken(
            Utilisateur utilisateur,
            AllowedIp allowedIp,
            AllowedNavigator allowedNavigator,
            UtilisateurRepository utilisateurRepository){

       Date date = new Date();
       Timestamp ts = new Timestamp(date.getTime());

       String generatedToken = DigestUtils.md5Hex(ts + utilisateur.getUserid() + "xQdqc" + allowedIp.getIp() + ts + allowedNavigator.getNavigateur() + utilisateur.getId()).toUpperCase();;

       utilisateur.setToken_validator(generatedToken);

       utilisateurRepository.save(utilisateur);


       return generatedToken;
    }

    /**
     * Génère le mot de passe unique de l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on génère un token
     * @param allowedIp adresse IP de l'utilisateur
     * @param allowedNavigator navigateur de l'utilisateur
     * @param utilisateurRepository repository de la base de données permettant de stocker la data
     * @return String le token généré
     */
    private String generateOTPToken(
            Utilisateur utilisateur,
            AllowedIp allowedIp,
            AllowedNavigator allowedNavigator,
            UtilisateurRepository utilisateurRepository){

        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());

        String uuid = UUID.randomUUID().toString();

        String generatedToken = DigestUtils.md5Hex(ts + utilisateur.getUserid() + "vvwPt%-_sts" + utilisateur.getUserid() + uuid + ts + "OtArIeMsPr" + allowedNavigator.getNavigateur() + "mSpRABSR$%[" + allowedIp.getIp()).toUpperCase();

        utilisateur.setToken_validator(generatedToken);

        utilisateurRepository.save(utilisateur);


        return generatedToken;
    }

    /**
     * Vérifie si le navigateur est validé pour l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on fait la recherche
     * @param navigateur navigateur utilisé par l'utilisateur
     * @param allowedNavigatorRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return boolean
     */
    private boolean isNavigatorValidated(Utilisateur utilisateur, String navigateur, AllowedNavigatorRepository allowedNavigatorRepository){
       List<AllowedNavigator> list = allowedNavigatorRepository.findByUtilisateurId(utilisateur.getId());

       for(AllowedNavigator allowedNavigator : list){
           if(allowedNavigator.getNavigateur().equals(navigateur))
               return true;
       }

       return false;
    }

    /**
     * Vérifie si l'adresse IP est validée pour l'utilisateur
     * @param utilisateur l'utilisateur pour lequel on fait la recherche
     * @param ipAddr adresse IP utilisée par l'utilisateur
     * @param allowedIpRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return boolean
     */
    private boolean ipIsValidated(Utilisateur utilisateur, String ipAddr, AllowedIpRepository allowedIpRepository){
       List<AllowedIp> list = allowedIpRepository.findByUtilisateurId(utilisateur.getId());

       for(AllowedIp allowedIp : list){
           if(allowedIp.getIp().equals(ipAddr))
               return true;
       }

       return false;
    }

    /**
     * Cette fonction permet de vérifier si le pays depuis lequel l'utilisateur se connecte est connu
     * @param utilisateur utilisateur concerné par la recherche
     * @param ipAddr adresse IP de l'utilisateur
     * @param allowedIpRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return boolean
     */
    private boolean isIpCountryKnown(Utilisateur utilisateur, AllowedIp ipAddr, AllowedIpRepository allowedIpRepository){
       List<AllowedIp> list = allowedIpRepository.findByUtilisateurId(utilisateur.getId());
        String newCountry = ipAddr.getCountryName();

        IPInfo ipInfo = IPInfo.builder().setToken("9c72b00404585e").build();

        try{
            String code;

           for(AllowedIp allowedIp : list){
               code = ipInfo.lookupIP(allowedIp.getIp()).getCountryCode();
               if(code != null && code.equals(newCountry))
                   return true;
           }

        }catch (RateLimitedException ex){
            LOGGER.error("Limite atteinte");
        }

       return false;
    }

    /**
     * Cette fonction permet de retrouver un utilisateur à partir de son userId
     * @param utilisateur utilisateur concerné par la recherche
     * @param utilisateurRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return Utilisateur l'ensemble des informations nécessaires
     */
    private Utilisateur retrieveUser(Utilisateur utilisateur, UtilisateurRepository utilisateurRepository){
        Iterable<Utilisateur> users = utilisateurRepository.findAll();

        for(Utilisateur user : users){
            System.out.println(user.getUserid());
            if(user.getUserid().equals(utilisateur.getUserid())){
                return user;
            }
        }

        return null;
    }

    /**
     * Permet d'identifier le navigateur utilisé par l'utilisateur
     * Inspiré par https://stackoverflow.com/questions/1326928/how-can-i-get-client-information-such-as-os-and-browser/18030465
     * @param userAgent signature du navigateur
     * @return String
     */
    private String getBrowser(String userAgent){
        String user =   userAgent.toLowerCase();

        String browser = "";

        System.out.println("User Agent for the request is===>"+userAgent);
        //===============Browser===========================
        if (user.contains("msie"))
        {
            browser="IE";
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser="SAFARI";
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera") || user.contains("opr"))
                browser="OPERA";
        } else if (user.contains("chrome"))
        {
            browser="CHROME";
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape";

        } else if (user.contains("firefox"))
        {
            browser="FIREFOX";
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = user;
        }
        System.out.println("Browser Name==========>"+browser);

        return browser;
    }
}
