package com.mspr.dev.controllers;

import com.mspr.dev.controller.OtpPost;
import com.mspr.dev.entities.Utilisateur;
import com.mspr.dev.entities.UtilisateurToken;
import com.mspr.dev.repositories.UtilisateurRepository;
import com.mspr.dev.repositories.UtilisateurTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * Controller de la page /otp
 */
@Controller
public class OtpPageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OtpPageController.class);

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private UtilisateurTokenRepository utilisateurTokenRepository;

    /**
     * Controller par lequel la requête GET /otp est gérée
     * @param model permet d'insérer des notions de datas qui seront mappées dans resources/templates
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String retourne un "String" qui correspond à un fichier html situé dans resources/templates
     */
    @GetMapping("/otp")
    public String otpForm(Model model, HttpServletRequest req) {

        model.addAttribute("otpPost", new OtpPost());
        return "otp";
    }

    /**
     * Controller par lequel la requête POST /otp est gérée
     * @param otpPost contient les informations saisies dans le formulaire OTP
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @param redirectAttributes permet d'ajouter des attributs à une prochaine redirection HTTP
     * @return String
     * @throws NamingException
     */
    @PostMapping("/otp")
    public String otpPost(OtpPost otpPost, HttpServletRequest req, RedirectAttributes redirectAttributes) throws NamingException {
        String token = checkToken(req, otpPost);
        if (token != null) {
            redirectAttributes.addAttribute("token", token);
            return "redirect:/success";
        }
        else
            return "otp";
    }

    /**
     * Controller par lequel la requête GET /success est gérée
     * @param token token de connexion généré
     * @param model permet d'insérer des notions de datas qui seront mappées dans resources/templates
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @return String
     */
    @GetMapping("/success")
    public String successGet(@ModelAttribute("token") String token, Model model, HttpServletRequest req) {
        model.addAttribute("token", token);
        return "success";
    }

    /**
     * Processus de vérification de la validité du OTP saisi
     * @param req élément nous permettant de travailler avec le contenu de la requête
     * @param otpPost contient les informations saisies dans le formulaire OTP
     * @return String token de connexion généré, permet d'authentifier la connexion à travers le réseau
     */
    private String checkToken(HttpServletRequest req, OtpPost otpPost){
        if(otpPost.getOtp() == null)
            return null;

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setToken_validator(otpPost.getOtp());


        Utilisateur usr = retrieveUser(utilisateur, utilisateurRepository);

        if(usr == null)
            return null;

        Date date = new Date(System.currentTimeMillis() + (4 * 60 * 60 * 1000));

        List<UtilisateurToken> list = utilisateurTokenRepository.findByUtilisateurId(usr.getId());

        UtilisateurToken utilisateurToken = new UtilisateurToken();
        utilisateurToken.setUtilisateur(usr);
        utilisateurToken.setExpire(date);
        utilisateurToken.setToken(utilisateurToken.generateToken());

        if(list.size() > 0){
           utilisateurToken.setId(list.get(list.size()-1).getId());
        }

        utilisateurTokenRepository.save(utilisateurToken);


        usr.setToken_validator(null);
        utilisateurRepository.save(usr);


        return utilisateurToken.getToken();
    }

    /**
     * Cette fonction permet de retrouver un utilisateur à partir de son OTP
     * @param utilisateur utilisateur concerné par la recherche
     * @param utilisateurRepository repository de la base de données permettant de stocker la data et d'y accéder
     * @return Utilisateur l'ensemble des informations nécessaires
     */
    private Utilisateur retrieveUser(Utilisateur utilisateur, UtilisateurRepository utilisateurRepository){
        Iterable<Utilisateur> users = utilisateurRepository.findAll();

        for(Utilisateur user : users){
            if(user.getToken_validator() != null && user.getToken_validator().equals(utilisateur.getToken_validator())){
                return user;
            }
        }

        return null;
    }
}
